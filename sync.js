/*
sync.js - syncs folder of albums containing images with Google Photos.
Heirachry should follow something like this:
d - albums
    d - Album Name 1
        i - Image File 001.jpg
        i - Image File 002.png
        v - Video File 001.mp4
    d - Album Name 2
        i - Image File 003.gif
*/

//set path to "albums" directory below
const albumsDir = process.argv[2];

if(typeof albumsDir === 'undefined') {
  console.log("Missing a passed albums directory please call this script again like this: node sync.js /path/to/your/pictures/folder");
  process.exit(1);
}

//DON'T MODIFY THE FILE BELOW THIS POINT UNLESS YOU KNOW WHAT YOU ARE DOING

//path to refresh token when authenticated.
const configFile = "./config.json";
//load the Google API Console Credentials File
const keys = require("./credentials.json");

//include the libraries
const fs = require("fs");
const _ = require("lodash");
const { OAuth2Client } = require("google-auth-library");
const http = require("http");
const url = require("url");
const querystring = require("querystring");
const opn = require("opn");
const readChunk = require("read-chunk");
const fileType = require("file-type");
const throttleLib = require("promise-ratelimit");
var ProgressBar = require("progress");



//trottleing section as part of Google's Spec which is to backoff after
//and error occurs. I have it broken into to parts
//1. The Albums Throttler
const AlbumRateMin = 5 * 1000; //5 seconds
let AlbumRate = AlbumRateMin;
const AlbumRateMax = 60 * 60 * 1000; //1 hour
const AlbumRateMultiplier = 1.5;
let AThrottle = throttleLib(AlbumRate);
//increase delay and throttle
const slowAlbums = async function() {
  let newAlbumRate = AlbumRate * AlbumRateMultiplier;
  if (AlbumRateMax > newAlbumRate) {
    AlbumRate = newAlbumRate;
  }
  AThrottle = throttleLib(AlbumRate);
  await AThrottle();
};
//reset throttle delay
const resetAlbumRate = function() {
  AlbumRate = AlbumRateMin;
  AThrottle = throttleLib(AlbumRate);
};
//actually apply the throttle
const TrottleAlbums = async function() {
  await AThrottle();
};

//2. The Photos Throttler
const PhotosRateMin = 2 * 1000; //2 seconds
let PhotosRate = PhotosRateMin;
const PhotosRateMax = 60 * 60 * 1000; //1 hour
const PhotosRateMultiplier = 1.5;
let PThrottle = throttleLib(PhotosRate);
//increase delay and trottle
const slowPhotos = async function() {
  let newPhotosRate = PhotosRate * PhotosRateMultiplier;
  if (PhotosRateMax > newPhotosRate) {
    PhotosRate = newPhotosRate;
  }
  PThrottle = throttleLib(PhotosRate);
  await PThrottle();
};
//reset throttle rate
const resetPhotosRate = function() {
  PhotosRate = PhotosRateMin;
  PThrottle = throttleLib(PhotosRate);
};
//actually apply the trottle
const TrottlePhotos = async function() {
  await PThrottle();
};

//Helper functions
//flow control helper function to iterate over an array using async awaits 
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
//returns the filesize in bytes for a particular file path
function getFilesizeInBytes(filename) {
  var stats = fs.statSync(filename);
  var fileSizeInBytes = stats["size"];
  return fileSizeInBytes;
}
//returns an array of directory/folder names given a filesystem path
//note note recursive
function getDirectories(path) {
  return fs.readdirSync(path).filter(function(file) {
    return fs.statSync(path + "/" + file).isDirectory();
  });
}
//returns an array of filenames given a filesystem path
//not not recursive
function getFiles(path) {
  return fs.readdirSync(path).filter(function(file) {
    return !fs.statSync(path + "/" + file).isDirectory();
  });
}
//strips out Amperstands or the "&" character and replaces it with the word "and";
function stripAmp(item) {
  if (typeof item === "string" && item.indexOf("&") > -1) {
    item = item.replace(/\&/g, "and");
  }
  return item;
}
//truncate string and add elipsis automatically.
var stringTruncate = function(str, length){
  var dots = str.length > length ? '...' : '';
  return str.substring(0, length)+dots;
};
//converts seconds to HH:mm:ss formatted string
function secondsToString(secconds) {
  var date = new Date(null);
  date.setSeconds(secconds);
  return date.toISOString().substr(11, 8);
}
//starts a process elasped time timer;
const start = process.hrtime();
//calculates the elapsed time and formats it using the secondsToString helper function
const elapsed_time = function() {
  return secondsToString(process.hrtime(start)[0]);
};

//the main app that manages syncing the files
async function main() {
  //unused bandwidth tracking tools
  let bandwidthUsage = 0;
  let startTime = Date.now();
  let currentDayBandwith = 0;
  const maxBandwidth = 10000; //10 GB
  let skippedCount = 0;
  
  //use Google Authentication library to handle opening an OAuth2Client connection
  const oAuth2Client = await getAuthenticatedClient();
  //monitor oauth2 token changes.
  oAuth2Client.on("tokens", tokens => {
    if (tokens.refresh_token) {
      //save the new tokens
      fs.writeFileSync(configFile, JSON.stringify(tokens));
    }
  });
  //declare a progresss bar format
  var bar = new ProgressBar(
    ":bar [:eString] :: (Albums)[:current/:total | :percent | E::AErrors, R::ARate%], (Photos)[:curPhotos/:totPhotos | :PhotosPercent% | S::skipped, I::ignored, B::tooBig | E::PErrors, R::PRate%] :LError",
    { total: 10 }
  );
  //declare starting progress bar tokens as an object that can be referenced later.
  let barUpdate = {
    AErrors: 0,//number of Album Errors
    ARate: Math.floor(
      (1 - (AlbumRate - AlbumRateMin) / (AlbumRateMax - AlbumRateMin)) * 100
    ),//Percent of Album Rate Performance
    curPhotos: 0,//current photos uploaded
    totPhotos: 0,//total photos to upload from album
    PhotosPercent: 0,//percent complete for that album
    PErrors: 0,//number of Photo Errors
    PRate: Math.floor(
      (1 - (PhotosRate - PhotosRateMin) / (PhotosRateMax - PhotosRateMin)) * 100
    ),//Percent of Photo Rate Performance
    eString: "00:00:00",//Elapsed String representing HH:mm:ss
    LError:'',//Last Error Message
    skipped:0,//number of skipped photos
    ignored:0,//number of ignored files
    tooBig:0,//number of too large of video or image files.
  };
  //update the elapsed time every second reguardless of other changes.
  setInterval(function() {
    barUpdate.eString = elapsed_time();
    bar.update(bar.curr / bar.total, barUpdate);
  }, 1000);
  //sync function that is wrapped to allow recalling it in the event of an error recursively with a delay.
  let uploader = async function() {
    try {
      // Make a simple request to the Google Photos API using our pre-authenticated client. The `request()` method
      // takes an AxiosRequestConfig object.  Visit https://github.com/axios/axios#request-config.
      //declare the api endpoints
      const albumsurl = "https://photoslibrary.googleapis.com/v1/albums";
      const photosurl =
        "https://photoslibrary.googleapis.com/v1/mediaItems:search";
        //declare default payload
      const params = {
        pageSize: 50
      };
      //make the actual request for albums
      let res = await oAuth2Client.request({ url: albumsurl, params });
      //store albums
      let galbums = res.data.albums;
      //look through albums list until no more album pages are available
      while (res.data.nextPageToken) {
        res = await oAuth2Client.request({
          method: "get",
          url: albumsurl,
          params: {
            pageToken: res.data.nextPageToken,
            pageSize: 50
          }
        });
        //structure of response is like below
        // albums =  [{id, title, mediaItemsCount, isWritable},...]
        //append new albums onto in memory storage of the Google Photos Albums
        galbums = galbums.concat(res.data.albums);
      }
      //get list of filesystem albums given the albums directory
      const albums = getDirectories(albumsDir);
      //update progress bar total with number of Filesystem Albums
      bar.total = albums.length;
      //iterate through all filesystem albums one by one.
      await asyncForEach(albums, async function(album, aindex) {
        //maintain proper rate via the Album Throttle
        await TrottleAlbums();
        //check Google Photos albums for a writable album based on the file system folder name formated without an "&"
        let existingAlbum = _.find(galbums, {
          title: stripAmp(album),
          isWriteable: true
        });
        //if no album is found create a new one.
        if (_.isNil(existingAlbum)) {
          //create album first;
          res = await oAuth2Client.request({
            method: "post",
            url: albumsurl,
            data: {
              album: { title: stripAmp(album) }
            }
          });
          //wont have mediaItemsCount but can be assumed zero for that property.
          res.data.mediaItemsCount = 0;
          existingAlbum = res.data;
          //add newly created album to reference database
          galbums.push(res.data);
        }
        //get photo and video filenames from the album folder
        const photos = getFiles(albumsDir + "/" + album);
        //update bar to reflect current number of photos
        barUpdate.totPhotos = photos.length;
        bar.update(bar.curr / bar.total, barUpdate);
        //prepare to build database of currently uploaded filenames
        let gphotos = [];
        //make sure the cloud album has photos in it
        if (existingAlbum.mediaItemsCount > 0) {
          //prepare first album listing request
          let albumParams = {
            pageSize: 100,
            albumId: existingAlbum.id
          };
          //make a request for album contents
          let res = await oAuth2Client.request({
            method: "post",
            url: photosurl,
            data: albumParams
          });
          //update cache of Google Photos albums
          gphotos = res.data.mediaItems;
          //continue to repeat this until the cache has been fully built with all the pages of photos.
          while (res.data.nextPageToken) {
            res = await oAuth2Client.request({
              method: "post",
              url: photosurl,
              data: {
                pageToken: res.data.nextPageToken,
                pageSize: 100,
                albumId: existingAlbum.id
              }
            });
            //append new page of photos to cache
            gphotos = gphotos.concat(res.data.mediaItems);
          }
        }
        //map the raw response objects to be just an array of filenames.
        gfilenames = _.map(gphotos, function(item) {
          return item.filename;
        });
        //build the actual working set for files that exist locally but not in the Google Photos album
        let workingset = _.differenceWith(photos, gfilenames, function(a, b) {
          return a === b;
        });
        //count how many photos are going to be skipped
        let skippingCount = photos.length - workingset.length;
        //if there are still some photos update the progress bar else update the progress bar anyways.
        if (skippingCount > 0 && skippedCount < photos.length) {
          barUpdate.curPhotos = skippingCount;
          barUpdate.skipped += skippingCount;
          barUpdate.PhotosPercent = Math.floor(
            (barUpdate.curPhotos / barUpdate.totPhotos) * 100
          );
          barUpdate.eString = elapsed_time();
          bar.update(bar.curr / bar.total, barUpdate);
          skippedCount += skippingCount;
        } else if (gfilenames.length === photos.length) {
          barUpdate.curPhotos = skippingCount;
          barUpdate.skipped += skippingCount;
          barUpdate.PhotosPercent = Math.floor(
            (barUpdate.curPhotos / barUpdate.totPhotos) * 100
          );
          barUpdate.eString = elapsed_time();
          bar.update(bar.curr / bar.total, barUpdate);
          skippedCount += skippingCount;
        }
        //build repeatable function for uploading the photo or video in the event of an error
        let uploadPhoto = async function(photo) {
          //handle throttling
          await TrottlePhotos();
          //build path to photo or video
          const filePath = albumsDir + "/" + album + "/" + photo;
          //get photo or video filesize in MB
          const fileSize = getFilesizeInBytes(filePath) / 1000000.0;
          //get buffer with first few bytes of the file to detect its filetype
          const buffer = readChunk.sync(filePath, 0, 4100);
          //check if file is an image if so check if it is larger than 50MB which is not supported with Google Photos
          if (fileType(buffer).mime.indexOf("image") > -1 && fileSize >= 50) {
            barUpdate.curPhotos = barUpdate.curPhotos++;
            barUpdate.tooBig++;
            barUpdate.PhotosPercent = Math.floor(
              (barUpdate.curPhotos / barUpdate.totPhotos) * 100
            );
            barUpdate.eString = elapsed_time();
            bar.update(bar.curr / bar.total, barUpdate);
            skippedCount++;
            return;
          }
          //check if file is a video and if so check if it is bigger than 5GB which is not allowed with the current API
          if (fileType(buffer).mime.indexOf("video") > -1 && fileSize >= 5000) {
            barUpdate.curPhotos = barUpdate.curPhotos++;
            barUpdate.tooBig++;
            barUpdate.PhotosPercent = Math.floor(
              (barUpdate.curPhotos / barUpdate.totPhotos) * 100
            );
            barUpdate.eString = elapsed_time();
            bar.update(bar.curr / bar.total, barUpdate);
            skippedCount++;
            return;
          }
          //check if the file is not supported mime type of an image or a video and skip it.
          if (fileType(buffer) == null || (fileType(buffer).mime.indexOf("video") === -1 && fileType(buffer).mime.indexOf("image") === -1)) {
            barUpdate.curPhotos = barUpdate.curPhotos++;
            barUpdate.ignored++;
            barUpdate.PhotosPercent = Math.floor(
              (barUpdate.curPhotos / barUpdate.totPhotos) * 100
            );
            barUpdate.eString = elapsed_time();
            bar.update(bar.curr / bar.total, barUpdate);
            skippedCount++;
            return;
          }
          //handle actual upload it it's own try catch to detect any errors with Google's API
          try {
            //refresh tokens just in case previous upload expired it
            const tokens = await oAuth2Client.getRequestHeaders();
            //make request to upload the binary data of the file
            res = await oAuth2Client.request({
              method: "post",
              url: "https://photoslibrary.googleapis.com/v1/uploads",
              data: fs.readFileSync(filePath),
              transformRequest: [
                function(data, headers) {
                  headers["Content-Type"] = "application/octet-stream";
                  headers["X-Goog-Upload-File-Name"] = photo;
                  headers["X-Goog-Upload-Protocol"] = "raw";
                  return data;
                }
              ],
              maxContentLength: 10485760000
            });
            //store the upload token
            let uploadToken = res.data;
            //make a request to add that file to a users library in the current album
            let addres = await oAuth2Client.request({
              method: "post",
              url:
                "https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate",
              data: {
                albumId: existingAlbum.id,
                newMediaItems: [
                  {
                    description: photo,
                    simpleMediaItem: {
                      uploadToken: uploadToken
                    }
                  }
                ]
              }
            });
            //update metrics
            currentDayBandwith += fileSize;
            bandwidthUsage += fileSize;
            //reset to default throttle rate after successful upload
            resetPhotosRate();
            //update progress bar
            barUpdate.curPhotos = barUpdate.curPhotos++;
            barUpdate.PhotosPercent = Math.floor(
              (barUpdate.curPhotos / barUpdate.totPhotos) * 100
            );
            barUpdate.eString = elapsed_time();
            barUpdate.PRate = Math.floor(
              (1 -
                (PhotosRate - PhotosRateMin) /
                  (PhotosRateMax - PhotosRateMin)) *
                100
            );
            //make sure to clear out any errors as now there isn't one
            barUpdate.LError = '';
            bar.update(bar.curr / bar.total, barUpdate);
          } catch (error) {
            //increase error counter
            barUpdate.PErrors++;
            barUpdate.PRate = Math.floor(
              (1 -
                (PhotosRate * PhotosRateMultiplier - PhotosRateMin) /
                  (PhotosRateMax - PhotosRateMin)) *
                100
            );
            barUpdate.eString = elapsed_time();
            //store the first 100 charaters of the error message
            barUpdate.LError = stringTruncate(JSON.stringify(error.message), 100);
            //update the progress bar
            bar.update(bar.curr / bar.total, barUpdate);
            //slow down the upload rate due to Google's recommended backoff procedure
            await slowPhotos();
            //queue a recursive upload of the same photo.
            await uploadPhoto(photo);
          }
        };
        //iterate through each unsynced filename in series.
        await asyncForEach(workingset, uploadPhoto);
        //reset throttle rate due to successful completion
        resetAlbumRate();
        //update progress bar
        barUpdate.eString = elapsed_time();
        barUpdate.ARate = Math.floor(
          (1 - (AlbumRate - AlbumRateMin) / (AlbumRateMax - AlbumRateMin)) * 100
        );
        //clear out any errors because we are in a successful state
        barUpdate.LError = '';
        bar.tick(barUpdate);
      });
    } catch (e) {
      //increase the error counter
      barUpdate.AErrors++;
      barUpdate.ARate = Math.floor(
        (1 -
          (AlbumRate * AlbumRateMultiplier - AlbumRateMin) /
            (AlbumRateMax - AlbumRateMin)) *
          100
      );
      barUpdate.eString = elapsed_time();
      //display the error
      barUpdate.LError = stringTruncate(e.message, 100);
      //update progress bar
      bar.update(bar.curr / bar.total, barUpdate);
      //slow down the album process due to errors
      await slowAlbums();
      //requeue the albums again reqursively
      await uploader();
    }
  };
  //run the albums for the first time
  await uploader();
  //sync is finished exit the app
  process.exit();
}


/**
 * Create a new OAuth2Client, and go through the OAuth2 content
 * workflow.  Return the full client to the callback.
 */
function getAuthenticatedClient() {
  return new Promise((resolve, reject) => {
    // create an oAuth client to authorize the API call.  Secrets are kept in a `keys.json` file,
    // which should be downloaded from the Google Developers Console.
    const oAuth2Client = new OAuth2Client(
      keys.web.client_id,
      keys.web.client_secret,
      keys.web.redirect_uris[0]
    );
    try {
      //attempt to read refresh token from config file
      let config = JSON.parse(fs.readFileSync(configFile));
      oAuth2Client.setCredentials(config);
      console.log("Using your previously saved authentication");
      resolve(oAuth2Client);
      return;
    } catch (error) {
      
    }

    // Generate the url that will be used for the consent dialog.
    const authorizeUrl = oAuth2Client.generateAuthUrl({
      access_type: "offline",
      scope: "https://www.googleapis.com/auth/photoslibrary",
      prompt: "consent"
    });

    // Open an http server to accept the oauth callback. In this simple example, the
    // only request to our webserver is to /oauth2callback?code=<code>
    const server = http
      .createServer(async (req, res) => {
        if (req.url.indexOf("/oauth2callback") > -1) {
          // acquire the code from the querystring, and close the web server.
          const qs = querystring.parse(url.parse(req.url).query);
          res.end("Authentication successful! Please return to the console.<script>window.close();</script>");
          server.close();

          // Now that we have the code, use that to acquire tokens.
          const r = await oAuth2Client.getToken(qs.code);
          //save tokens to the config file
          fs.writeFileSync(configFile, JSON.stringify(r.tokens));
          // Make sure to set the credentials on the OAuth2 client.
          oAuth2Client.setCredentials(r.tokens);
          console.info("Authentication saved to your config file.");
          resolve(oAuth2Client);
        }
      })
      .listen(3000, () => {
        // open the browser to the authorize url to start the workflow
        opn(authorizeUrl);
      });
  });
}

//actually run the app.
main();
