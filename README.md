
# Folders 2 Google Photos

A Node.JS CLI app that takes a folder hierarchy and does a one way sync with a Google Photos account automatically creating Albums in the most efficient way I can think of currently.

![screenshot](https://i.imgur.com/jxK3xSR.png)

# Usage
In Terminal you can run the script with the following as long as you cd into the project directory

    node sync.js ../albums

The Albums folder needs to be of a similar structure to the following:
- d - albums
	- d - Album Name 1
		- i - Image File 001.jpg
		- i - Image File 002.png
		- v - Video File 001.mp4
	 - d - Album Name 2
	   	- i - Image File 003.gif

where "d -" are directories or folders and "I -" and "v -"  are images and videos respectively

# installation

 1. install Node.JS if you don't already have it.
 2. Clone this repo
 3. Get a Google Photos API Credentials.json file
     1. GO Here: [https://developers.google.com/photos/library/guides/get-started](https://developers.google.com/photos/library/guides/get-started)
	 2. click the blue "Enabled The Google Photos API" button if your signed into your google account
	 3. precede through the steps
	 4. when it asks you to configure the OAuth client choose webserver
	 5. for the Authorized redirect URIs field put in this `http://localhost:3000/oauth2callback`
	 6. download the client configuration and add it to the repo directory you cloned in step 1
 4. open terminal and change into the repo directory using `cd /path/to/directory`
 5. run the following to install dependencies: `npm install`
 6. run the app with the following syntax: `node sync.js /path/to/albums/directory`

# Contribute
feel free to make a pull request with any changes you would like to make.

# Requirements
This has only been tested on macOS but should work on linux and potentially even windows if someone is up to test it on those Operating Systems. It needs a browser to handle the initial handshake with your Google Photos account.
- NOTE: it supports 2 factor authentication or 2FA since it uses oath2 tokens.

# Limitations 

 - Google photos only allows 10,000 requests per day for just talking with their api given your api credentials.
 - Google Files doesn't work with images over 50MB
 - Google Photos API has a hidden limit of 5GB per day so I restrict video files to that size.
 - changes made to files without changing their filename results in Google Photos not have those changes.
 - Changes made to the albums on Google Photos may be overwritten
 - Existing albums named the same as your filesystem will not be writable through the Google Photos API so a new album will be created so that the app can sync that album resulting in duplicate albums on Google Photos.
 - This app only syncs up to Google Photos and does not sync from Google Photos down to your computer. It is not Bi-directional.
 - it will only sync mime type files of images and videos anything outside of that is ignored.

# License
This project is licensed under the MIT License
